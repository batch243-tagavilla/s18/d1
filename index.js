// console.log("Good afternoon");

// Parameters and Arguments

    function printName(name="noName") {
        console.log("My name is " + name); 
    }

    printName("Chris");
    printName("George");
    printName();

    // You can directly pass data into the function. The function can then use that data which is referred as "name" within the function.
    // "name" is called parameter.
    // parameter acts as named variable/container that exists only inside of a function.
    // "Chris" is the information/data provided directly into the function called an argument.
    // Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

    // Variables can also be passed as an argument.
    let sampleVariable = "Edward";
    printName(sampleVariable);

    // Function arguments cannot be used by a function if there are no parameters provided within the function.
    function noParams() {
        let params = "No parameters";

        console.log(params);
    }

    noParams("with params");

    function checkDivisibilityBy8(num) {
        let modulo = num%8;
        console.log("The remainder of " + num + " divided by 8 is " + modulo);

        let isDivisibleBy8 = (modulo === 0);
        console.log("Is " + num + " divisible by 8? ");
        console.log(isDivisibleBy8);
    }

    checkDivisibilityBy8(8);
    checkDivisibilityBy8(17);

    // You can also do the same using the prompt(), however. take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

    // Functions as Arguments 
        // function parameters can also accpet other functions as arguments.
        // Some complex funtions use other functions as arguements to perform more complicated results.

        function argumentFunction() {
            console.log("This function was passed as an argument before the message was printed.")
        }

        function argumentFunction2() {
            console.log("This function was passed as an argument from the second argument function.")
        }

        function invokeFunction(argFunction) {
            argFunction();
        }

        invokeFunction(argumentFunction);
        invokeFunction(argumentFunction2);

        // Adding and removing the parentheses "()" impacts the output of JavaScript heavily.
        // When function is used with parentheses "()", it denotes invoking a function.
        // A function used without parentheses "()" is normally associated with using the function as an argument to another function.

    // Using multiple parameters
        // Multiple "arguements" will correpsond to the number of "parameters" declared in a function in "succeeding order".

        function createFullName(firstName, middleName="noMiddleName", lastName) {
            console.log("This is first name: " + firstName);
            console.log("This is middle name: " + middleName);
            console.log("This is last name: " + lastName);
        }

        createFullName("Juan", "Dela", "Cruz");
        createFullName("Juan", "Dela", "Cruz", "Jr.");
        createFullName("Juan", undefined, "Cruz");

        // Using variables as arguments

        let firstName = "John";
        let middleName = "Doe";
        let lastName = "Smith";

        createFullName(firstName, middleName, lastName);

// Return Statement
    // Allows us to output a value from a function to be passed to the line/block of code that invoked the function.

    function returnFullName(firstName, middleName, lastName) {
        console.log(firstName + " " + middleName + " " + lastName);
    }

    returnFullName("Ada", "None", "Lovelace");

    function returnName(firstName, middleName, lastName) {
        return (firstName + " " + middleName + " " + lastName);
    }

    console.log(returnName("John", "Doe", "Smith"));

    function printPlayerInfo(userName, level, job) {
        console.log("Username: " + userName);
        console.log("Level: " + level);
        console.log("Job: " + job)

        return userName + " " + level + " " + job;
    }

    let user1 = printPlayerInfo("knight_white", 95, "Paladin");
    console.log(user1);